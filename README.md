# Authenticate to GitLab Maven Repository to Consume Artifacts

This example demonstrates how to authenticate and consume an Android AAR or dependency from a [GitLab Maven Repository](https://docs.gitlab.com/ee/user/packages/maven_repository/). With the combination of a GitLab Maven Repository and Gradle, AAR dependencies can be consumed from a [group](https://docs.gitlab.com/ee/user/packages/maven_repository/#group-level-maven-endpoint) or [project](https://docs.gitlab.com/ee/user/packages/maven_repository/#project-level-maven-endpoint) level for reuse across teams.

## Requirements
* Public GitLab Project or Silver/Premium License

## Usage

First, a [maven repository](https://docs.gradle.org/current/userguide/declaring_repositories.html#sec:declaring_multiple_repositories) block must be added to `allprojects` in the parent gradle file of the Android project.
Within the block, a url pointing to the project hosting the dependencies must be declared. Additionally, credentials must be specified to gain access to the maven repository.
These [credentials](https://docs.gradle.org/current/dsl/org.gradle.api.artifacts.repositories.AuthenticationSupported.html) can be a [ci-token](https://docs.gitlab.com/ee/user/packages/maven_repository/#authenticating-with-a-ci-job-token) or [personal access token](https://docs.gitlab.com/ee/user/packages/maven_repository/#authenticating-with-a-personal-access-token).

Variables:
* `PROJECT_ID`: GitLab project ID storing the AAR dependency
* `CI_JOB_TOKEN`: Pre-defined variable included in GitLab pipeline

```script
allprojects {
    repositories {
        google()
        jcenter()

        maven {
            name "gitlab-maven"
            url "https://gitlab.com/api/v4/projects/{PROJECT_ID}/packages/maven"

             credentials(HttpHeaderCredentials) {
                name = "Job-Token"
                value = System.getenv('CI_JOB_TOKEN')
            }
            authentication {
                header(HttpHeaderAuthentication)
            }
        }
    }
}
```

Finally, add the [android implementation](https://developer.android.com/studio/build/dependencies#dependency-types) line to import the dependency to your project. Gradle will pull the file from GitLab Maven repository and provide access to library definitions.

```script
dependencies {
    implementation 'com.example:library:0.1'
}
```




## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


